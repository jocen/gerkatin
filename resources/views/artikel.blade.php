@extends('layout')

@section('content')
	<div class="css-artikel">
		<div class="banner-artikel" style="background: url('{{asset('images/artikel.jpg')}}');">
			<div class="tbl">
				<div class="cell">
					<div class="container">
						<div class="box">
							<div class="bg"></div>
							<div class="bg2"></div>
							<div class="t">Dapatkan info terbaru dari GERKATIN</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	    <div class="pad120">
		    <div class="container">
		    	<div class="css-mix">
			    	<div class="row">
			    		<div class="col-md-3 col-lg-2 xs30">
			    			<div class="t no-bdr">Kategori</div>
			    			<ul class="controls">
					            <li class="control" data-filter="all">Semua</li>
					            <li class="control" data-filter=".Kegiatan">Kegiatan</li>
					            <li class="control" data-filter=".Workshop">Workshop</li>
					            <li class="control" data-filter=".Pelatihan">Pelatihan</li>
					        </ul>
						</div>
						<div class="col-md-9 col-lg-9">
							<div class="t bdr">Judul Artikel</div>
							<ul class="content-mix">
					            <li class="mix Kegiatan">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Kegiatan</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Kegiatan">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Kegiatan</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Workshop">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Workshop</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Pelatihan">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Pelatihan</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Kegiatan">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Kegiatan</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Workshop">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Workshop</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Pelatihan">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Pelatihan</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					            <li class="mix Workshop">
					            	<div class="tbl">
					            		<div class="cell img">
					            			<img src="{{asset('images/artikel.jpg')}}" alt="" title=""/>
					            		</div>
					            		<div class="cell">
					            			<div class="text">Workshop</div>
					            			<div class="date">20 Oktober 2020  |  Sumber:</div>
					            			<div class="bdr-btm"></div>
					            			<div class="title">Mengadakan PORTRIN setiap 3 tahun sekali</div>
					            			<div class="arrow"><i class="fas fa-chevron-right"></i> <i class="fas fa-chevron-down"></i>Baca Lanjut</div>
					            			<div class="bdy">
					            				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat.</p>
					            			</div>
					            		</div>
					            	</div>
					            </li>
					        </ul>
						</div>
					</div>
				</div>
		    </div>
		</div>
	</div>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('mixitup.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<script type="text/javascript">
    $(function() {
    	$('.nav-artikel').addClass('active');

    	var containerEl = document.querySelector('.content-mix');

		var mixer = mixitup(containerEl);

		$('.css-mix ul.content-mix li .tbl .cell .arrow').click(function(event) {
			// $('.css-mix ul.content-mix li .tbl .cell .arrow').removeClass('active');
			$(this).toggleClass('active');
			$(this).parents('.cell').find('.bdy').toggleClass('active');
		});
    });
</script>
@endsection