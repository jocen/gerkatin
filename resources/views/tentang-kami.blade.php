@extends('layout')

@section('css')
<link href="{{ asset('slick/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('slick/slick-theme.css') }}" rel="stylesheet"/>
@endsection

@section('content')
	<div class="css-tentang-kami">
		<div class="box-sejarah" style="background: url('{{asset('images/sejarah.png')}}');">
	    	<div class="container">
	    		<div class="row">
	    			<div class="col-md-5 col-lg-6 xs20">
	    				<div class="img"><img src="{{asset('images/sejarah2.png')}}" alt="" title=""/></div>
	    			</div>
	    			<div class="col-md-7 col-lg-6">
	    				<div class="t1">Sejarah</div>
	    				<div class="t2">Gerkatin</div>
	    				<div class="t3">
	    					<p>Sebelumnya ada beberapa komunitas organisasi tuna rungu Indonesia yang bersifat kedaerahan yang telah terbentuk pada tahun 1960 antara lain: Bandung dengan nama SEKATUBI (Serikat Kaum Tuli Bisu Indonesia), PTRS (Persatuan Tuna Rungu Semarang), Jogyakarta PERTRI (Perhimpunan Tuna Rungu Indonesia), PEKATUR (Perkumpulan Kaum Tuli Surabaya). Sehubungan banyaknya komunitas organisasi tuna rungu yang bersifat kedaerahan, maka beberapa pimpinan organisasi tersebut sepakat mengadakan Kongres Nasional I pada tanggal 23 Februari 1981 di Jakarta. Hasil Kongres telah menghasilkan beberapa keputusan diantaranya menyempurnakan nama organisasi menjadi satu yaitu GERKATIN kepanjangan dari Gerakan untuk Kesejahteraan Tuna Rungu Indonesia dalam bahasa inggrisnya yaitu IAWD (indonesian Association for the Welfare of the Deaf). Dalam perkembangan selanjutnya, GERKATIN/IAWD telah terdaftar sejak tahun 1983 sebagai anggota WFD (World Federation of the Deaf) diindonesiakan Federasi Tuna Rungu se-Dunia bermarkas di Helsinki, Finlandia.</p>
	    				</div>
	    			</div>
	    		</div>
	    	</div>
	    </div>

	    <div class="pad120">
		    <div class="container">
		    	<div class="box-pendahuluan">
			    	<div class="row">
			    		<div class="col-lg-6 order-lg-1 order-2 my-auto sr-left-td1">
			    			<div class="t1">Pendahuluan</div>
			    			<div class="bdy">
			    				<p>Tuna Rungu atau Tuli adalah seseorang yang kehilangan daya pendengaran sejak kelahiran disebabkan oleh takdir dan faktor lainnya (Sakit, musibah, kecelakaan, lanjut usia). Orang tuna rungu/tuli sudah jelas banyak menerima ketertinggalan di berbagai informasi), komunikasi dari mulut ke mulut juga terhalang, walau di sisi yang sangat tidak menguntungkan tetapi ada pepatah mengatakan “raga boleh cacat asal jiwanya tidak cacat” inilah yang memberi kami bersemangat untuk mengejar ketertinggalan dan kami sanggup menyamai kesetaraan dengan orang yang berpendengar melalui pendidikan yang akses bervisualisasi antara lain membaca bibir, menulis, membaca teks berjalan dan berkomunikasi menggunakan bahasa isyarat. </p>
			    			</div>
			    		</div>
			    		<div class="col-lg-5 offset-lg-1 order-lg-2 order-1 my-auto sr-right-td1">
			    			<div class="img"><img src="{{asset('images/pendahuluan.png')}}" alt="" title=""/></div>
			    		</div>
			    	</div>
			    </div>
		    </div>
		</div>

		<div class="box-kolaborasi">
			<div class="container">
				<div class="row">
					<div class="col-md-5 col-lg-6 my-auto">
						<div class="bg">
							<div class="tbl">
								<div class="cell">
									<ul class="nav nav-kolaborasi" role="tablist">
										<li>
											<a class="active" id="visi-tab" data-toggle="tab" href="#visi" role="tab" aria-controls="visi" aria-selected="true">
												<div class="wtext">Visi</div>
												<div class="icon">
													<img class="icon1" src="{{asset('images/visi1.png')}}" alt="" title=""/>
													<img class="icon2" src="{{asset('images/visi2.png')}}" alt="" title=""/>
												</div>
											</a>
										</li>
										<li>
											<a id="misi-tab" data-toggle="tab" href="#misi" role="tab" aria-controls="misi" aria-selected="false">
												<div class="wtext">Misi</div>
												<div class="icon">
													<img class="icon1" src="{{asset('images/misi1.png')}}" alt="" title=""/>
													<img class="icon2" src="{{asset('images/misi2.png')}}" alt="" title=""/>
												</div>
											</a>
										</li>
										<li>
											<a id="tujuan-tab" data-toggle="tab" href="#tujuan" role="tab" aria-controls="tujuan" aria-selected="false">
												<div class="wtext">Tujuan</div>
												<div class="icon">
													<img class="icon1" src="{{asset('images/tujuan1.png')}}" alt="" title=""/>
													<img class="icon2" src="{{asset('images/tujuan2.png')}}" alt="" title=""/>
												</div>
											</a>
										</li>
										<li>
											<a id="landasan-hukum-tab" data-toggle="tab" href="#landasan-hukum" role="tab" aria-controls="landasan-hukum" aria-selected="false">
												<div class="wtext">Landasan Hukum</div>
												<div class="icon">
													<img class="icon1" src="{{asset('images/landasan1.png')}}" alt="" title=""/>
													<img class="icon2" src="{{asset('images/landasan2.png')}}" alt="" title=""/>
												</div>
											</a>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-7 col-lg-6 my-auto">
						<div class="pl50">
							<div class="tab-content">
								<div class="tab-pane fade show active" id="visi" role="tabpanel" aria-labelledby="visi-tab">
									<div class="bdy-tab">
										<p>Mencapai kesetaraan kesempatan dalam semua aspek kehidupan dan penghidupan</p><br/>
										<p>Menciptakan organisasi tuna rungu yang madani</p><br/>
										<p>Menjadi organisasi Nasional yang bermitra dengan Pemerintah dan non pemerintah untuk mewujudkan tercapainya kesetaraan dalam kesempatan, meningkatkan kesejahteraan dan kompetensi tunarungu dalam segala aspek kehidupan dan penghidupan.</p>
									</div>
								</div>
								<div class="tab-pane fade" id="misi" role="tabpanel" aria-labelledby="misi-tab">
									<div class="bdy-tab">
										<p>Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat.</p><br/>
										<p>Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p>
									</div>
								</div>
								<div class="tab-pane fade" id="tujuan" role="tabpanel" aria-labelledby="tujuan-tab">
									<div class="bdy-tab">
										<p>Consequat occaecat ullamco amet non eiusmod nostrud dolore irure incididunt est duis anim sunt officia.</p><br/>
										<p>Fugiat velit proident aliquip nisi incididunt nostrud exercitation proident est nisi. Irure magna elit commodo anim ex veniam culpa eiusmod id nostrud sit cupidatat in veniam ad.</p><br/>
										<p>Eiusmod consequat eu adipisicing minim anim aliquip cupidatat culpa excepteur quis. Occaecat sit eu exercitation irure Lorem incididunt nostrud.</p>
									</div>
								</div>
								<div class="tab-pane fade" id="landasan-hukum" role="tabpanel" aria-labelledby="landasan-hukum-tab">
									<div class="bdy-tab">
										<p>Et et consectetur ipsum labore excepteur est proident excepteur ad velit occaecat qui minim occaecat veniam. Fugiat veniam incididunt anim aliqua enim pariatur veniam sunt est aute sit dolor anim. Velit non irure adipisicing aliqua ullamco irure incididunt irure non esse consectetur nostrud minim non minim occaecat.</p><br/>
										<p>Amet duis do nisi duis veniam non est eiusmod tempor incididunt tempor dolor ipsum in qui sit. Exercitation mollit sit culpa nisi culpa non adipisicing reprehenderit do dolore. Duis reprehenderit occaecat anim ullamco ad duis occaecat ex.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="pad120">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">Prestasi GERKATIN</div>
		    		<div class="slider-prestasi">
		    			<div class="item">
		    				<p>Jakarta Indonesia, melalui GERKATIN terpilih sebagai tuan rumah oleh Federasi Tunarungu se-Dunia kawasan Asia Pasifik untuk menyelenggarakan Pertemuan Delegasi Tunarungu se-Asia Pasifik ke XVI bulan desember 2004 di Hotel Sari Pan Pasifik dan dihadiri oleh 14 negara peserta serta dibuka resmi oleh, Menteri Sosial R.I, Bapak Bachtiar Chamsyah SE.</p>
		    			</div>
		    			<div class="item">
		    				<p>Jakarta Indonesia, melalui GERKATIN terpilih sebagai tuan rumah oleh Federasi Tunarungu se-Dunia kawasan Asia Pasifik untuk menyelenggarakan Pertemuan Delegasi Tunarungu se-Asia Pasifik ke XVI bulan desember 2004 di Hotel Sari Pan Pasifik dan dihadiri oleh 14 negara peserta serta dibuka resmi oleh, Menteri Sosial R.I, Bapak Bachtiar Chamsyah SE.</p>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		</div>

		<div class="pad120 pb0">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">Prestasi GERKATIN</div>
		    		<div class="row justify-content-center">
		    			<div class="col-md-10 col-lg-8">
				    		<div class="t2">
				    			<p>Apabila anda mempunyai pertanyaan mengenai kelas, kolaborasi, serta permintaan narasumber atau wawancara.Dapat mengirimkan Email atau hubungi kami melalui Whatsapp</p>
				    		</div>
				    	</div>
				    </div>
		    	</div>
		    </div>
    		<div class="img-calender">
    			<img src="{{asset('images/calender.jpg')}}"/>
    		</div>
		</div>

		<div class="pad120 pb0">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">JUDUL VIDEO</div>
		    		<div class="row justify-content-center">
		    			<div class="col-md-10 col-lg-8">
				    		<div class="t2">
				    			<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,</p>
				    		</div>
				    	</div>
				    </div>
					<div class="slider-video">
						<div class="item">
							<iframe src="https://www.youtube.com/embed/0tC4deUeY0g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
						<div class="item">
							<iframe src="https://www.youtube.com/embed/0tC4deUeY0g" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
						</div>
					</div>
		    	</div>
		    </div>
		</div>

		<div class="pad120 pb0">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">Struktur GERKATIN</div>
		    		<div class="slider-struktur">
		    			<div class="item">
		    				<div class="t2">DPP Gerkatin periode 2015 - 2020</div>
		    				<div class="text-center">
			    				<div class="t3">Dewan Pembina pusat</div>
			    			</div>
		    				<div class="t4">
		    					<p>Bapak Direktur Orang dengan Disabilitas KEMENSOS R.I</p>
		    					<p>Bapak Ketua Dewan Nasional Indonesia Kesejahteraan Sosial</p>
		    					<p>Bapak Ketua Umum Perkumpulan Penyandang Disabilitas Indonesia</p>
		    				</div>
				    		<div class="img-line">
				    			<img src="{{asset('images/line1.png')}}"/>
				    		</div>
				    		<div class="row">
		    					<div class="col-md-12 col-lg-8 offset-lg-2">
						    		<div class="row">
						    			<div class="col-md-4 offset-md-4 order-2 order-md-1">
						    				<div class="profile50">
									    		<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
									    		<div class="posisi-profile">Ketua</div>
									    	</div>
						    			</div>
						    			<div class="col-md-4 order-1 order-md-2 xs20">
						    				<div class="img-line2">
								    			<img src="{{asset('images/line2.png')}}"/>
								    		</div>
						    				<div class="profile50">
									    		<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
									    		<div class="posisi-profile">Dewan Pertimbangan Organisasi Pusat</div>
									    	</div>
						    			</div>
						    		</div>
						    	</div>
						    </div>
				    		<div class="img-line">
				    			<img src="{{asset('images/line1.png')}}"/>
				    		</div>
				    		<div class="profile50">
					    		<div class="img-profile">
					    			<img src="{{asset('images/profile.png')}}"/>
					    		</div>
					    		<div class="nm-profile">Nama Lengkap</div>
					    		<div class="posisi-profile">Wakil Ketua</div>
					    	</div>
					    	<div class="img-line">
				    			<img src="{{asset('images/line3.png')}}"/>
				    		</div>
				    		<div class="row">
		    					<div class="col-md-12 col-lg-8 offset-lg-2">
				    				<div class="row">
				    					<div class="col-md-6">
				    						<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Sekretarias</div>
								    		</div>
							    			<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Wakil Sekretarias</div>
					    					</div>
				    					</div>
				    					<div class="col-md-6">
				    						<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Bendahara</div>
								    		</div>
								    		<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Wakil Bendahara</div>
					    					</div>
				    					</div>
				    				</div>
				    			</div>
				    		</div>
		    			</div>
		    			<div class="item">
		    				<div class="t2">DPP Gerkatin periode 2015 - 2020</div>
		    				<div class="text-center">
			    				<div class="t3">Dewan Pembina pusat</div>
			    			</div>
		    				<div class="t4">
		    					<p>Bapak Direktur Orang dengan Disabilitas KEMENSOS R.I</p>
		    					<p>Bapak Ketua Dewan Nasional Indonesia Kesejahteraan Sosial</p>
		    					<p>Bapak Ketua Umum Perkumpulan Penyandang Disabilitas Indonesia</p>
		    				</div>
		    				<div class="img-line">
				    			<img src="{{asset('images/line1.png')}}"/>
				    		</div>
				    		<div class="row">
		    					<div class="col-md-12 col-lg-8 offset-lg-2">
						    		<div class="row">
						    			<div class="col-md-4 offset-md-4 order-2 order-md-1">
						    				<div class="profile50">
									    		<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
									    		<div class="posisi-profile">Ketua</div>
									    	</div>
						    			</div>
						    			<div class="col-md-4 order-1 order-md-2 xs20">
						    				<div class="img-line2">
								    			<img src="{{asset('images/line2.png')}}"/>
								    		</div>
						    				<div class="profile50">
									    		<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
									    		<div class="posisi-profile">Dewan Pertimbangan Organisasi Pusat</div>
									    	</div>
						    			</div>
						    		</div>
						    	</div>
						    </div>
					    	<div class="img-line">
				    			<img src="{{asset('images/line1.png')}}"/>
				    		</div>
				    		<div class="profile50">
					    		<div class="img-profile">
					    			<img src="{{asset('images/profile.png')}}"/>
					    		</div>
					    		<div class="nm-profile">Nama Lengkap</div>
					    		<div class="posisi-profile">Wakil Ketua</div>
					    	</div>
					    	<div class="img-line">
				    			<img src="{{asset('images/line3.png')}}"/>
				    		</div>
				    		<div class="row">
		    					<div class="col-md-12 col-lg-8 offset-lg-2">
				    				<div class="row">
				    					<div class="col-md-6">
				    						<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Sekretarias</div>
								    		</div>
							    			<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Wakil Sekretarias</div>
					    					</div>
				    					</div>
				    					<div class="col-md-6">
				    						<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Bendahara</div>
								    		</div>
								    		<div class="mb20">
					    						<div class="img-profile">
									    			<img src="{{asset('images/profile.png')}}"/>
									    		</div>
									    		<div class="nm-profile">Nama Lengkap</div>
								    			<div class="posisi-profile">Wakil Bendahara</div>
					    					</div>
				    					</div>
				    				</div>
				    			</div>
				    		</div>
		    			</div>
		    		</div>
		    	</div>
		    </div>
		</div>
		<div class="pad120 pb0">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">Bidang di DPD dan DPC </div>
		    		<div class="row justify-content-center">
		    			<div class="col-md-10 col-lg-8">
				    		<div class="t2">
				    			<p>Berikut ini bidang-bidang setiap aspek yang dikepalai tokoh pengurus sebagai berikut: </p>
				    		</div>
				    	</div>
				    </div>
		    	</div>
		    	<div class="slider-bidang">
		    		<div class="item">
		    			<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    		</div>
		    		</div>
		    		<div class="item">
		    			<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
		    						<div class="img-profile">
						    			<img src="{{asset('images/profile.png')}}"/>
						    		</div>
						    		<div class="nm-profile">
						    			<div>BIDANG</div> 
						    			<div>Advokasi & Kesetaraan</div>
						    		</div>
					    			<div class="posisi-profile">Nama Lengkap</div>
			    				</div>
			    			</div>
			    		</div>
		    		</div>
		    	</div>
		    </div>
		</div>

		<div class="pad120 pb0">
		    <div class="container">
		    	<div class="box-struktur">
		    		<div class="t">Struktur Kepengurusan GERKATIN</div>
		    		<div class="row justify-content-center">
		    			<div class="col-md-10 col-lg-8">
				    		<div class="t2">
				    			<p>Apabila anda mempunyai pertanyaan mengenai kelas, kolaborasi, serta permintaan narasumber atau wawancara.Dapat mengirimkan Email atau hubungi kami melalui Whatsapp</p>
				    		</div>
				    	</div>
				    </div>
		    	</div>
		    	<div class="slider-gerkatin2">
		    		<div class="item">
		    			<div class="text-center">
			    			<div class="t2">DPD GERKATIN</div>
			    		</div>
			    		<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    	<div class="item">
		    			<div class="text-center">
			    			<div class="t2">DPD GERKATIN</div>
			    		</div>
			    		<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-struktur">
			    						<div class="img-profile">
							    			<img src="{{asset('images/profile.png')}}"/>
							    		</div>
							    		<div class="nm-profile">Nama Lengkap</div>
						    			<div class="posisi-profile">Ketua DPD Banten</div>
			    					</a>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    </div>
			</div>
		</div>

		<div class="pad120">
		    <div class="container">
		    	<div class="slider-gerkatin">
		    		<div class="item">
		    			<div class="text-center">
			    			<div class="t2">Daftar Anggota DPC GERKATIN</div>
				    		<div class="row justify-content-center">
				    			<div class="col-md-10 col-lg-8">
						    		<div class="t3">
						    			<p>Apabila anda mempunyai pertanyaan mengenai kelas, kolaborasi, serta permintaan narasumber atau wawancara.Dapat mengirimkan Email atau hubungi kami melalui Whatsapp</p>
						    		</div>
						    	</div>
						    </div>
			    		</div>
			    		<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd banten</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd Jakarta</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa tengah</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd banten</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd Jakarta</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa tengah</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa timur</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd bali</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd nusa tenggara barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd nusa tenggara timur</a>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
			    	<div class="item">
		    			<div class="text-center">
			    			<div class="t2">Daftar Anggota DPC GERKATIN</div>
			    			<div class="row justify-content-center">
				    			<div class="col-md-10 col-lg-8">
						    		<div class="t3">
						    			<p>Apabila anda mempunyai pertanyaan mengenai kelas, kolaborasi, serta permintaan narasumber atau wawancara.Dapat mengirimkan Email atau hubungi kami melalui Whatsapp</p>
						    		</div>
						    	</div>
						    </div>
			    		</div>
			    		<div class="row">
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd banten</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd Jakarta</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa tengah</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd banten</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd Jakarta</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa tengah</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd jawa timur</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd bali</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd nusa tenggara barat</a>
			    				</div>
			    			</div>
			    			<div class="col-6 col-md-3">
			    				<div class="item-gerkatin">
			    					<a data-toggle="modal" data-target="#modal-daftar-anggota">Dpd nusa tenggara timur</a>
			    				</div>
			    			</div>
			    		</div>
			    	</div>
		    	</div>
		    </div>
		</div>
	</div>

<div class="modal fade" id="modal-daftar-anggota" tabindex="-1" role="dialog" aria-labelledby="modal-daftar-anggota" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="close-pop" data-dismiss="modal"><i class="fas fa-times"></i></div>
            <div class="t-pop">ANGGOTA DPC GERKATIN</div>
            <div class="row justify-content-center">
            	<div class="col-md-10 col-lg-9">
					<div class="bdy-pop">
						<p>ketua Dewan Pengurus Daerah (DPD) DKI Jakarta membawahi susunan Dewan Pengurus Cabang (DPC) sebagai berikut:</p>
					</div>
				</div>
			</div>
            <div class="row justify-content-center">
            	<div class="col-6 col-md-3">
            		<ul class="bdy-name">
            			<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
					</ul>
				</div>
				<div class="col-6 col-md-3">
            		<ul class="bdy-name">
            			<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
					</ul>
				</div>
				<div class="col-6 col-md-3">
            		<ul class="bdy-name">
            			<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
					</ul>
				</div>
				<div class="col-6 col-md-3">
            		<ul class="bdy-name">
            			<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
						<li>Nama orang</li>
					</ul>
				</div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-struktur" tabindex="-1" role="dialog" aria-labelledby="modal-struktur" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="close-pop" data-dismiss="modal"><i class="fas fa-times"></i></div>
            <div class="t-pop">DPD GERKATIN DKI JAKARTA</div>
            <div class="row justify-content-center">
            	<div class="col-md-10 col-lg-9">
					<div class="bdy-pop">
						<p>Dewan Pengurus Daerah (DPD) DKI Jakarta membawahi susunan Dewan Pengurus Cabang (DPC) sebagai berikut:</p>
					</div>
				</div>
			</div>
            <div class="row justify-content-center">
            	<div class="col-6 col-md-4 col-lg-3">
            		<div class="pop-gerkatin">
						<div class="img-profile">
			    			<img src="{{asset('images/profile.png')}}"/>
			    		</div>
			    		<div class="nm-profile">Nama Lengkap</div>
		    			<div class="posisi-profile">Ketua DPC Jakarta Barat</div>
    				</div>
				</div>
				<div class="col-6 col-md-4 col-lg-3">
            		<div class="pop-gerkatin">
						<div class="img-profile">
			    			<img src="{{asset('images/profile.png')}}"/>
			    		</div>
			    		<div class="nm-profile">Nama Lengkap</div>
		    			<div class="posisi-profile">Ketua DPC Jakarta Utara</div>
    				</div>
				</div>
				<div class="col-6 col-md-4 col-lg-3">
            		<div class="pop-gerkatin">
						<div class="img-profile">
			    			<img src="{{asset('images/profile.png')}}"/>
			    		</div>
			    		<div class="nm-profile">Nama Lengkap</div>
		    			<div class="posisi-profile">Ketua DPC Jakarta Selatan</div>
    				</div>
				</div>
				<div class="col-6 col-md-4 col-lg-3">
            		<div class="pop-gerkatin">
						<div class="img-profile">
			    			<img src="{{asset('images/profile.png')}}"/>
			    		</div>
			    		<div class="nm-profile">Nama Lengkap</div>
		    			<div class="posisi-profile">Ketua DPC Jakarta Timur</div>
    				</div>
				</div>
				<div class="col-6 col-md-4 col-lg-3">
            		<div class="pop-gerkatin">
						<div class="img-profile">
			    			<img src="{{asset('images/profile.png')}}"/>
			    		</div>
			    		<div class="nm-profile">Nama Lengkap</div>
		    			<div class="posisi-profile">Ketua DPC Jakarta Pusat</div>
    				</div>
				</div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<script type="text/javascript">
    $(function() {
    	$('.nav-tentang-kami').addClass('active');

    	$('.slider-video').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
            dots: false,
            centerMode: false,
            arrows: false,
            infinite: true,
            autoplay: true,
            autoplaySpeed: 1000,
            speed: 1000,
            responsive: [
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 580,
                settings: {
                    slidesToShow: 1,
                }
            }
            ]
		});

    	$('.slider-gerkatin').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: false,
		});

		$('.slider-gerkatin2').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: false,
		});

		$('.slider-prestasi').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: false,
		});

		$('.slider-struktur').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: false
		});

		$('.slider-bidang').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: false,
		});
    });
</script>
@endsection