<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=2.0"/>
    <meta name="description" content="Gerkatin">
    <meta name="keywords" content="Gerkatin">
    <title>GERKATIN</title>
    <!--favicon-->
    <link rel="apple-touch-icon" sizes="180x180" href="{{asset('images/favicon180x180.png')}}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{asset('images/favicon16x16.png')}}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{asset('images/favicon32x32.png')}}">
    
    <!-- CSS -->
    @yield('css')
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('fontawesome/css/all.css') }}">
    <link href="{{ asset('css/fonts.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/front.css?v.5') }}" rel="stylesheet"/>

</head>
<body>
    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v9.0&appId=1409463772627039&autoLogAppEvents=1" nonce="X17Y12KF"></script>

    <h1 style="display:none;">Gerkatin</h1>
    
    <header>
        <div class="header-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-9 col-md-4 my-auto">
                        <div class="link">
                            <a href="{{ URL::to('/') }}">
                                <div class="logo">
                                    <img src="{{asset('images/logo.png?v.1')}}" alt="" title=""/>
                                </div>
                                <div class="txt-logo">
                                    <div class="t1">Gerkatin</div>
                                    <div class="t2">Gerakan untuk Kesejahteraan Tunarungu Indonesia</div>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-3 col-md-8 my-auto text-right">
                        <div class="menu">
                            <i class="fas fa-bars"></i>
                        </div>
                        <div class="main-menu hidden-xs">
                            <nav class="nav-menu">
                                <a href="{{ URL::to('/') }}" data-scroll="beranda">Beranda</a>
                                <a href="{{ URL::to('/#program') }}" data-scroll="program">Program</a>
                                <a href="{{ URL::to('/artikel') }}" class="nav-artikel">Artikel</a>
                                <a href="{{ URL::to('/tentang-kami') }}" class="nav-tentang-kami">Tentang Kami</a>
                                <a href="{{ URL::to('/#kontak-kami') }}" data-scroll="kontak-kami">Kontak Kami</a>
                                <a href="{{ URL::to('/donasi') }}">Donasi</a>
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="main">
        @yield('content')
    </div>

    <footer class="getMenu" id="kontak">
        <div class="container">
            <div class="bdr">
                <div class="row">
                    <div class="col-md-7 col-lg-6 md30">
                        <div>
                            <div class="tbl tbl-footer">
                                <div class="cell img-cell">
                                    <div class="img">
                                        <img src="{{asset('images/logo-footer.png')}}"/>
                                    </div>
                                </div>
                                <div class="cell">
                                    <div class="t-footer uppercase">Gerkatin</div>
                                    <div class="mb40">
                                        <div class="bdy-footer pr30">
                                            <p>Jl. Rancho Indah Dalam No. 47 BC Tanjung Barat - Jakarta Selatan</p>
                                        </div>
                                    </div>
                                    <div class="mb40">
                                        <div class="t-footer">Pusat Informasi</div>
                                        <ul class="l-footer">
                                            <li><a href="#" target="_blank" rel="noreferrer noopener"><span class="icon"><img src="{{asset('images/wa.png')}}"/></span> +6281907900275 (Hanya Chat)</a></li>
                                            <li><a href="mailto:gerkatin@yahoo.com"><span class="icon"><img src="{{asset('images/mail.png')}}"/></span> gerkatin@yahoo.com</a></li>
                                        </ul>
                                    </div>
                                    <div class="t-footer">Social Media</div>
                                    <ul class="l-soc">
                                        <li><a href="https://www.instagram.com/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-instagram"></i></span></a></li>
                                        <li><a href="https://www.facebook.com/" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-facebook-f"></i></span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-lg-3 md30">
                        <div class="mb20">
                            <div class="t-footer">Mitra</div>
                            <div>
                                <ul class="l-soc l-img">
                                    <li><div class="img"><img src="{{asset('images/logo-pusbisindo.png')}}"/></div></li>
                                    <li><div class="img"><img src="{{asset('images/logo-plj.png')}}"/></div></li>
                                </ul>
                            </div>
                        </div>
                        <div class="mb20">
                            <div class="t-footer">Kerja Sama</div>
                            <div>
                                <ul class="l-soc l-img">
                                    <li><div class="img"><img src="{{asset('images/logo-wfotd.png')}}"/></div></li>
                                    <li><div class="img"><img src="{{asset('images/logo-wfd.png')}}"/></div></li>
                                </ul>
                            </div>
                        </div>
                        <div>
                            <div class="t-footer">Donatur</div>
                            <div>
                                <ul class="l-soc l-img l-small">
                                    <li><div class="img"><img src="{{asset('images/logo-drf.png')}}"/></div></li>
                                    <li><div class="img"><img src="{{asset('images/logo-nippon.png')}}"/></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3">
                        <div class="bdy-footer">
                            <p>Dukung dan berpartisipasilah dalam pelaksanaan program kegiatan kami dengan menjadi donatur. Untuk melakukan donasi silahkan klik tombol dibawah ini :</p>
                        </div>
                        <div class="btn-donasi"><a href="#">Donasi</a></div>
                        <ul class="l-soc l-donasi">
                            <li><div class="img"><img src="{{asset('images/logo-mandiri.png')}}"/></div></li>
                            <li><div class="img"><img src="{{asset('images/logo-bca.png')}}"/></div></li>
                            <li><div class="img big"><img src="{{asset('images/logo-paypal.png')}}"/></div></li>
                        </ul>
                    </div>
                </div>
                <div class="cp">Copyright &copy; <a href="https://api.whatsapp.com/send?phone=628977707814" target="_blank">stefanstar</a> <?php echo date("Y"); ?></div>
            </div>
        </div>
    </footer>

    <div class="fixed-top">
        <a>
            <div class="tbl">
                <div class="cell">
                    <div class="text">
                        <i class="fas fa-chevron-up"></i>
                        <div>TOP</div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="fixed-message">
        <a href="#" target="_blank" rel="noreferrer noopener">
            <div class="tbl">
                <div class="cell">
                    <div class="txt">
                        <div>Informasi &</div>
                        <div>Pengaduan</div>
                    </div>
                    <div class="img">
                        <img src="{{asset('images/fix-phone.png?v.1')}}"/>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="fixed-donasi">
        <a href="#" target="_blank" rel="noreferrer noopener">
            <div class="tbl">
                <div class="cell">
                    <div class="txt">Donasi</div>
                    <div class="img">
                        <img src="{{asset('images/fix-donasi.png?v.1')}}"/>
                    </div>
                </div>
            </div>
        </a>
    </div>

    <div class="overlay"></div>
    <div class="slide-menu">
        <div class="box-close">
            <i class="fas fa-times"></i>
        </div>
        <div class="pad20">
           <nav class="nav-menu2">
                <a href="{{ URL::to('/') }}" data-scroll="beranda">Beranda</a>   
                <a href="{{ URL::to('/#program') }}" data-scroll="program">Program</a>
                <a href="{{ URL::to('/artikel') }}" data-scroll="artikel">Artikel</a>
                <a href="{{ URL::to('/tentang-kami') }}" data-scroll="tentang-kami">Tentang Kami</a>
                <a href="{{ URL::to('/#kontak-kami') }}" data-scroll="kontak-kami">Kontak Kami</a>
                <a href="{{ URL::to('/donasi') }}">Donasi</a>
            </nav>
        </div>
    </div>


<script type="text/javascript" src="{{ asset('jquery-3.5.1.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
<script type="text/javascript" src="{{ asset('web.js') }}"></script>

<!-- JS -->
@yield('js')

</body>
</html>
        
