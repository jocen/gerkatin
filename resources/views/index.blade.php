@extends('layout')

@section('css')
<link href="{{ asset('slick/slick.css') }}" rel="stylesheet"/>
<link href="{{ asset('slick/slick-theme.css') }}" rel="stylesheet"/>
@endsection

@section('content')
	<section id="beranda" data-anchor="beranda">
    	<div class="slider-banner">
    		<div class="item" style="background: url('{{asset('images/beranda.jpg')}}');">
    			<div class="abs-banner"></div>
    			<div class="tbl">
    				<div class="cell">
    					<div class="container">
			    			<div class="t1">Membuka akses ke dunia tuli</div>
			    			<div class="t2">
			    				<p>Kita memberikan kesetaraan hak penyandang Tuli / Tunarungu dalam</p>
			    				<p>mengakses segala aspek di kehidupan bermasyarakat Indonesia</p>
			    			</div>
			    			<div class="t3">
			    				<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Cari Tahu</button></a>
			    			</div>
			    		</div>
		    		</div>
		    	</div>
    		</div>
    		<div class="item" style="background: url('{{asset('images/beranda.jpg')}}');">
    			<div class="abs-banner"></div>
    			<div class="tbl">
    				<div class="cell">
    					<div class="container">
			    			<div class="t1">Membuka akses ke dunia tuli</div>
			    			<div class="t2">
			    				<p>Kita memberikan kesetaraan hak penyandang Tuli / Tunarungu dalam</p>
			    				<p>mengakses segala aspek di kehidupan bermasyarakat Indonesia</p>
			    			</div>
			    			<div class="t3">
			    				<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Cari Tahu</button></a>
			    			</div>
			    		</div>
		    		</div>
		    	</div>
    		</div>
    		<div class="item" style="background: url('{{asset('images/beranda.jpg')}}');">
    			<div class="abs-banner"></div>
    			<div class="tbl">
    				<div class="cell">
    					<div class="container">
			    			<div class="t1">Membuka akses ke dunia tuli</div>
			    			<div class="t2">
			    				<p>Kita memberikan kesetaraan hak penyandang Tuli / Tunarungu dalam</p>
			    				<p>mengakses segala aspek di kehidupan bermasyarakat Indonesia</p>
			    			</div>
			    			<div class="t3">
			    				<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Cari Tahu</button></a>
			    			</div>
			    		</div>
		    		</div>
		    	</div>
    		</div>
    		<div class="item" style="background: url('{{asset('images/beranda.jpg')}}');">
    			<div class="abs-banner"></div>
    			<div class="tbl">
    				<div class="cell">
    					<div class="container">
			    			<div class="t1">Membuka akses ke dunia tuli</div>
			    			<div class="t2">
			    				<p>Kita memberikan kesetaraan hak penyandang Tuli / Tunarungu dalam</p>
			    				<p>mengakses segala aspek di kehidupan bermasyarakat Indonesia</p>
			    			</div>
			    			<div class="t3">
			    				<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Cari Tahu</button></a>
			    			</div>
			    		</div>
		    		</div>
		    	</div>
    		</div>
    	</div>
    </section>

    <section id="program" data-anchor="program">
    	<div class="pad120">
    		<div class="container">
    			<div class="box-program">
	    			<div class="row">
	    				<div class="col-md-7 colg-lg-6 offset-lg-1 sr-left-td1">
	    					<div class="title-global">Program Kerja</div>
	    					<div class="bdy-global left w450">
	    						<p>Untuk menunjang pengembangan sumber daya manusia. Disediakan berbagai aspek program kerja di Gerkatin sebagai berikut:</p>
	    					</div>
	    					<div class="tbl-program">
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Aksesbilitas</div>
	    								<div class="cell">Hubungan Masyarakat</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Kesejahteraan</div>
	    								<div class="cell">Kepemudaan dan Olah raga</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Kesehatan</div>
	    								<div class="cell">Organisasi</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Tenaga Kerja</div>
	    								<div class="cell">Hubungan Luar Negeri</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Kewanitaan</div>
	    								<div class="cell">Hubungan Internasional</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Seni Budaya</div>
	    								<div class="cell">Hubungan Masyarakat</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Pendidikan</div>
	    								<div class="cell">Bahasa Isyarat Alamiah Indonesia</div>
	    							</div>
	    						</div>
	    						<div>
	    							<div class="tbl">
	    								<div class="cell w200">Advokasi</div>
	    								<div class="cell">(BISINDO)</div>
	    							</div>
	    						</div>
	    					</div>
	    				</div>
	    				<div class="col-md-5 col-lg-4 sr-right-td1">
	    					<div class="clearfix">
	    						<div class="float-img">
	    							<img src="{{asset('images/program1.jpg')}}"/>
	    						</div>
	    						<div class="float-img">
	    							<img src="{{asset('images/program2.jpg')}}"/>
	    						</div>
	    						<div class="float-img">
	    							<img src="{{asset('images/program3.jpg')}}"/>
	    						</div>
	    						<div class="float-img">
	    							<img src="{{asset('images/program1.jpg')}}"/>
	    						</div>
	    						<div class="float-img">
	    							<img src="{{asset('images/program2.jpg')}}"/>
	    						</div>
	    						<div class="float-img">
	    							<img src="{{asset('images/program3.jpg')}}"/>
	    						</div>
	    					</div>
	    				</div>
	    			</div>
    			</div>
    		</div>
    	</div>
    </section>

    <div class="box-sejarah" style="background: url('{{asset('images/sejarah.png')}}');">
    	<div class="container">
    		<div class="row">
    			<div class="col-md-5 col-lg-6 xs20">
    				<div class="img"><img src="{{asset('images/sejarah2.png')}}"/></div>
    			</div>
    			<div class="col-md-7 col-lg-6">
    				<div class="t1">Sejarah</div>
    				<div class="t2">Gerkatin</div>
    				<div class="t3">
    					<p>Sebelumnya ada beberapa komunitas organisasi tuna rungu Indonesia yang bersifat kedaerahan yang telah terbentuk pada tahun 1960 antara lain: Bandung dengan nama SEKATUBI (Serikat Kaum Tuli Bisu Indonesia), PTRS (Persatuan Tuna Rungu Semarang), Jogyakarta PERTRI (Perhimpunan Tuna Rungu Indonesia), PEKATUR (Perkumpulan Kaum Tuli Surabaya). Sehubungan banyaknya komunitas organisasi tuna rungu yang bersifat kedaerahan, maka beberapa pimpinan organisasi tersebut sepakat mengadakan Kongres Nasional I pada tanggal 23 Februari 1981 di Jakarta. Hasil Kongres telah menghasilkan beberapa keputusan diantaranya menyempurnakan nama organisasi menjadi satu yaitu GERKATIN kepanjangan dari Gerakan untuk Kesejahteraan Tuna Rungu Indonesia dalam bahasa inggrisnya yaitu IAWD (indonesian Association for the Welfare of the Deaf). Dalam perkembangan selanjutnya, GERKATIN/IAWD telah terdaftar sejak tahun 1983 sebagai anggota WFD (World Federation of the Deaf) diindonesiakan Federasi Tuna Rungu se-Dunia bermarkas di Helsinki, Finlandia.</p>
    				</div>
    				<div class="t4">
    					<a href="{{ URL::to('/tentang-kami') }}"><button type="button" class="hvr-button">Lanjut Baca</button></a>
    				</div>
    			</div>
    		</div>
    	</div>
    </div>

    <div class="pad120">
	    <div class="container">
	    	<div class="title-global text-center mb40">Kontribusi</div>
	    	<div class="numbers row">
	    		<div class="col-md-4">
					<div class="item-kontribusi">
						<div class="t1">Kepada</div>
						<div class="value">7</div>
						<div class="text">Juta lebih</div>
						<div class="bdy">
							<p>Penyandang Tuli /</p>
							<p>Tunarungu</p>
						</div>
					</div>	
				</div>
				<div class="col-md-4 bdr-kontribusi">
					<div class="item-kontribusi">
						<div class="t1">Cabang</div>
						<div class="value">30</div>
						<div class="text">Dpd</div>
						<div class="bdy">
							<p>Bekerja sama dengan 30</p>
							<p>DPD dan xx DPC</p>
						</div>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="item-kontribusi">
						<div class="t1">Mengadakan</div>
						<div class="pos-rel">
							<div class="value">11</div>
							<div class="plus">+</div>
						</div>
						<div class="text">Kegiatan</div>
						<div class="bdy">
							<p>Berupa workshop, Pelatihan, Kongres, dan lain-lain dalam kurun tahun 2004 - sekarang</p>
						</div>
					</div>	
				</div>
			</div>

			<!-- <div class="row">
				<div class="col-md-4">
					<div class="item-kontribusi">
						<div class="t1">Kepada</div>
						<div class="timer count-number" data-to="7" data-speed="700"></div>
						<div class="text">Juta lebih</div>
						<div class="bdy">
							<p>Penyandang Tuli /</p>
							<p>Tunarungu</p>
						</div>
					</div>	
				</div>
				<div class="col-md-4 bdr-kontribusi">
					<div class="item-kontribusi">
						<div class="t1">Cabang</div>
						<div class="timer count-number" data-to="30" data-speed="700"></div>
						<div class="text">Dpd</div>
						<div class="bdy">
							<p>Bekerja sama dengan 30</p>
							<p>DPD dan xx DPC</p>
						</div>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="item-kontribusi">
						<div class="t1">Mengadakan</div>
						<div class="pos-rel">
							<div class="timer count-number" data-to="11" data-speed="700"></div>
							<div class="plus">+</div>
						</div>
						<div class="text">Kegiatan</div>
						<div class="bdy">
							<p>Berupa workshop, Pelatihan, Kongres, dan lain-lain dalam kurun tahun 2004 - sekarang</p>
						</div>
					</div>	
				</div>
			</div> -->
	    </div>
	</div>

	<div class="box-kolaborasi">
		<div class="container">
			<div class="row">
				<div class="col-md-5 col-lg-6">
					<div class="bg">
						<div class="tbl">
							<div class="cell">
								<div class="t">Kolaborasi</div>
								<div class="t2">
									<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain </p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-7 col-lg-6">
					<div class="pl50">
						<div class="slider-kolaborasi">
							<div class="item">
								<div class="t-kolaborasi">Mitra</div>
								<ul class="l-kolaborasi">
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-pusbisindo2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Menjalin kerja sama dalam advokasi bahasa isyarat, terutama dalam pelatihan, penelitian dan seminar mengenai pentingnya akses bahasa isyarat dalam semua lapisan masyarakat</p>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-plj2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain </p>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-drf2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain </p>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
							<div class="item">
								<div class="t-kolaborasi">Mitra</div>
								<ul class="l-kolaborasi">
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-pusbisindo2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Menjalin kerja sama dalam advokasi bahasa isyarat, terutama dalam pelatihan, penelitian dan seminar mengenai pentingnya akses bahasa isyarat dalam semua lapisan masyarakat</p>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-plj2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain </p>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="tbl">
											<div class="cell w150">
												<div class="img"><img src="{{asset('images/logo-drf2.png')}}"/></div>
											</div>
											<div class="cell">
												<div class="bdy">
													<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain </p>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="pad120 pb0">
		<div class="container">
			<div class="bg-follow">
				<div class="t-program">Follow Us:</div>
				<ul class="l-soc">
	                <li><a href="https://www.facebook.com//" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-facebook-f"></i></span></a></li>
	                <li><a href="https://www.instagram.com//" target="_blank" rel="noreferrer noopener"><span class="icon"><i class="fab fa-instagram"></i></span></a></li>
	            </ul>
	        </div>
			<div class="title-global text-center">Informasi Terkini</div>
			<div class="row justify-content-center">
    			<div class="col-md-10 col-lg-8">
					<div class="bdy-global mb15">
						<p>Membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain</p>
					</div>
				</div>
			</div>
		</div>
		<div class="box-info">
			<div class="container">
				<div class="slider-info">
					<div class="item">
						<a href="{{ URL::to('/artikel') }}">
							<div class="img"><img src="{{asset('images/info.jpg')}}"/></div>
							<div class="in">
								<div class="t1">Kegiatan</div>
								<div class="t2">Mengadakan PORTRIN setiap 3 tahun sekali</div>
								<div class="t3">
									<p>Mengadakan PORTRIN setiap 3 tahun sekali. Diadakan untuk mempererat hubungan antar daerah DPC membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain</p>
								</div>
								<div class="t4">> lanjut baca</div>
							</div>
						</a>
					</div>
					<div class="item">
						<a href="{{ URL::to('/artikel') }}">
							<div class="img"><img src="{{asset('images/info.jpg')}}"/></div>
							<div class="in">
								<div class="t1">Kegiatan</div>
								<div class="t2">Mengadakan PORTRIN setiap 3 tahun sekali</div>
								<div class="t3">
									<p>Mengadakan PORTRIN setiap 3 tahun sekali. Diadakan untuk mempererat hubungan antar daerah DPC membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain</p>
								</div>
								<div class="t4">> lanjut baca</div>
							</div>
						</a>
					</div>
					<div class="item">
						<a href="{{ URL::to('/artikel') }}">
							<div class="img"><img src="{{asset('images/info.jpg')}}"/></div>
							<div class="in">
								<div class="t1">Kegiatan</div>
								<div class="t2">Mengadakan PORTRIN setiap 3 tahun sekali</div>
								<div class="t3">
									<p>Mengadakan PORTRIN setiap 3 tahun sekali. Diadakan untuk mempererat hubungan antar daerah DPC membuka kesempatan bagi kalangan Tuli untuk menerima lebih banyak akses baik itu akses komunikasi, informasi, pekerjaan dan lain-lain</p>
								</div>
								<div class="t4">> lanjut baca</div>
							</div>
						</a>
					</div>
					<div class="item">
						<a href="{{ URL::to('/artikel') }}">
							<div class="img"><img src="{{asset('images/info.jpg')}}"/></div>
							<div class="in">
								<div class="t1">Kegiatan</div>
								<div class="t2">Mengadakan PORTRIN setiap 3 tahun sekali</div>
								<div class="t3">
									<p>Mengadakan PORTRIN setiap 3 tahun sekali. Diadakan untuk mempererat hubungan antar daerah DPC....</p>
								</div>
								<div class="t4">> lanjut baca</div>
							</div>
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="pad120 pb0">
	    <div class="container">
	    	<div class="title-global text-center mb40">Mari buat perbedaan</div>
			<div class="row">
				<div class="col-md-4 xs30">
					<div class="item-beda">
						<a href="{{ URL::to('/tentang-kami') }}">
							<div class="img"><img src="{{asset('images/beda1.png')}}"/></div>
							<div class="text">Kenali lebih dalam</div>
							<div class="bdy">
								<p>Tidak kenal maka tidak sayang. Mari mengetahui lebih banyak tentang kami serta berpartisipasi dengan kita untuk berkontribusi lebih banyak.</p>
							</div>
						</a>
					</div>	
				</div>
				<div class="col-md-4 xs30">
					<div class="item-beda">
						<a class="click-kontak-kami">
							<div class="img"><img src="{{asset('images/beda2.png')}}"/></div>
							<div class="text">Hubungi kita</div>
							<div class="bdy">
								<p>Untuk mengajukan proposal atau mendapatkan informasi lebih detail, dapat menghubungi kita melalui chat whatsapp ( tidak terima telepon )</p>
							</div>
						</a>
					</div>	
				</div>
				<div class="col-md-4">
					<div class="item-beda">
						<a href="#">
							<div class="img"><img src="{{asset('images/beda3.png')}}"/></div>
							<div class="text">Donasi</div>
							<div class="bdy">
								<p>Memberi donasi berarti mendukung gerakan kita untuk membantu lebih banyak bagi yang membutuhkan saluran bantuan kita</p>
							</div>
						</a>
					</div>	
				</div>
			</div>
	    </div>
	</div>

	<section id="kontak-kami" data-anchor="kontak-kami">
		<div class="pad120">
			<div class="container">
				<div class="title-global text-center">Kontak Kami</div>
				<div class="row justify-content-center">
	    			<div class="col-md-10 col-lg-9">
			    		<div class="bdy-global">
			    			<p>Apabila anda mempunyai pertanyaan mengenai kelas, kolaborasi, serta permintaan narasumber atau wawancara. Dapat mengirimkan Email atau hubungi kami melalui Whatsapp</p>
			    		</div>
			    	</div>
			    </div>
	    		<div class="box-tentang-kami">
		    		<div class="row">
		    			<div class="col-md-6 col-lg-7 xs20">
		    				<div class="img">
				    			<img src="{{asset('images/tentang-kami.jpg')}}" alt="" title=""/>
				    		</div>
		    			</div>
		    			<div class="col-md-6 col-lg-5">
		    				<div class="mb30">
			    				<div class="t-kami">Alamat Surat</div>
			    				<div class="bdy-kami">
			    					<p class="mb15">Jl. Rancho Indah Dalam No. 47 BC Tanjung Barat - Jakarta Selatan</p>
			    					<p><span class="bold">Catatan:</span> Yang ingin mengunjungi kantor PUSBISINDO, diharapkan untuk membuat janji terlebih dahulu melalui Email atau Whatsapp.</p>
			    				</div>
			    			</div>
			    			<div>
			    				<div class="t-kami">Informasi Kontak</div>
		    					<ul class="l-kami">
		    						<li class="img"><img src="{{asset('images/fax.png')}}" alt="" title=""/></li>
		    						<li>021-7892668</li>
		    					</ul>
		    					<ul class="l-kami">
		    						<li class="img"><img src="{{asset('images/email.png')}}" alt="" title=""/></li>
		    						<li>proposal@gerkatin.org</li>
		    					</ul>
		    					<ul class="l-kami">
		    						<li class="img"><img src="{{asset('images/website.png')}}" alt="" title=""/></li>
		    						<li>www.gerkatin.org</li>
		    					</ul>
		    					<ul class="l-kami">
		    						<li class="img"><img src="{{asset('images/phone.png')}}" alt="" title=""/></li>
		    						<li>+6281807900275</li>
		    					</ul>
			    			</div>
		    			</div>
		    		</div>
		    	</div>
			</div>
		</div>
	</section>
@endsection

@section('js')
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('scrollreveal.min.js') }}"></script>

<script type="text/javascript">

	function widthMaxbdy(){
		if ($(window).width() > 1024) {
			$('.slider-info .item a .in .t3 p').each(function() {
		        $(this).text($(this).text().substr(0, 90) + '...');
		    });
		}
		else if ($(window).width() > 767) {
			$('.slider-info .item a .in .t3 p').each(function() {
		        $(this).text($(this).text().substr(0, 70) + '...');
		    });
		}
	}
    $(function() {
    	$('.slider-banner').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			centerMode: false,
			arrows: false,
			infinite: true,
			autoplay: false
		});

	    $('.slider-info').slick({
			slidesToShow: 3,
			slidesToScroll: 1,
			dots: false,
			centerMode: false,
			arrows: true,
			infinite: true,
			autoplay: true,
			responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
				}
			}
			]
		});

		$('.slider-kolaborasi').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			dots: true,
			centerMode: false,
			arrows: false,
			infinite: true,
			autoplay: false
		});

		widthMaxbdy();

		$(window).on('resize', function(){
			widthMaxbdy();

		});
    });

    (function ($) {
		$.fn.countTo = function (options) {
			options = options || {};
			
			return $(this).each(function () {
				// set options for current element
				var settings = $.extend({}, $.fn.countTo.defaults, {
					from:            $(this).data('from'),
					to:              $(this).data('to'),
					speed:           $(this).data('speed'),
					refreshInterval: $(this).data('refresh-interval'),
					decimals:        $(this).data('decimals')
				}, options);
				
				// how many times to update the value, and how much to increment the value on each update
				var loops = Math.ceil(settings.speed / settings.refreshInterval),
					increment = (settings.to - settings.from) / loops;
				
				// references & variables that will change with each update
				var self = this,
					$self = $(this),
					loopCount = 0,
					value = settings.from,
					data = $self.data('countTo') || {};
				
				$self.data('countTo', data);
				
				// if an existing interval can be found, clear it first
				if (data.interval) {
					clearInterval(data.interval);
				}
				data.interval = setInterval(updateTimer, settings.refreshInterval);
				
				// initialize the element with the starting value
				render(value);
				
				function updateTimer() {
					value += increment;
					loopCount++;
					
					render(value);
					
					if (typeof(settings.onUpdate) == 'function') {
						settings.onUpdate.call(self, value);
					}
					
					if (loopCount >= loops) {
						// remove the interval
						$self.removeData('countTo');
						clearInterval(data.interval);
						value = settings.to;
						
						if (typeof(settings.onComplete) == 'function') {
							settings.onComplete.call(self, value);
						}
					}
				}
				
				function render(value) {
					var formattedValue = settings.formatter.call(self, value, settings);
					$self.html(formattedValue);
				}
			});
		};
		
		$.fn.countTo.defaults = {
			from: 0,               // the number the element should start at
			to: 0,                 // the number the element should end at
			speed: 1000,           // how long it should take to count between the target numbers
			refreshInterval: 100,  // how often the element should be updated
			decimals: 0,           // the number of decimal places to show
			formatter: formatter,  // handler for formatting the value before rendering
			onUpdate: null,        // callback method for every time the element is updated
			onComplete: null       // callback method for when the element finishes updating
		};
		
		function formatter(value, settings) {
			return value.toFixed(settings.decimals);
		}
	}(jQuery));

	jQuery(function ($) {
	  // custom formatting example
	  $('.count-number').data('countToOptions', {
		formatter: function (value, options) {
		  return value.toFixed(options.decimals).replace(/\B(?=(?:\d{3})+(?!\d))/g, ',');
		}
	  });
	  
	  // start all the timers
	  $('.timer').each(count);  
	  
	  function count(options) {
		var $this = $(this);
		options = $.extend({}, options || {}, $this.data('countToOptions') || {});
		$this.countTo(options);
	  }
	});

	jQuery(document).ready(function($) {
		$(window).scroll(testScroll);
		var viewed = false;

		function isScrolledIntoView(elem) {
		    var docViewTop = $(window).scrollTop();
		    var docViewBottom = docViewTop + $(window).height() + 220;

		    var elemTop = $(elem).offset().top;
		    var elemBottom = elemTop + $(elem).height();

		    return ((elemBottom <= docViewBottom) && (elemTop >= docViewTop));
		}

		function testScroll() {
		  if (isScrolledIntoView($(".numbers")) && !viewed) {
		      viewed = true;
		      $('.value').each(function () {
			      $(this).prop('Counter',0).animate({
					Counter: $(this).text()
					}, {
						duration: 700,
						easing: 'swing',
						step: function (now) {
							$(this).text(Math.ceil(now));
						}
					});
				});
			}
		}
	});
</script>
@endsection