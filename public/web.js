function scrollAll() {
    a = $(window).scrollTop();
    if (a > 150) {
        $('.fixed-message').fadeIn();
        $('.fixed-top').fadeIn();
        $('.fixed-donasi').fadeIn();
    } else {
        $('.fixed-message').fadeOut();
        $('.fixed-top').fadeOut();
        $('.fixed-donasi').fadeOut();
    }
}

function clickMenu(){
    if ($(window).width() > 1024) {
      $('.slider-info .item a .in .t3 p').each(function() {
            $(this).text($(this).text().substr(0, 90) + '...');
        });
    }
    else if ($(window).width() > 767) {
      $('.slider-info .item a .in .t3 p').each(function() {
            $(this).text($(this).text().substr(0, 70) + '...');
        });
    }
  }

$(function() {
    $('.menu').click(function(event) {
      $('body').addClass('no-scroll');
      $('.overlay').addClass('active');
      $('.slide-menu').addClass('active');
  });

  $('.box-close').click(function(event) {
      $('body').removeClass('no-scroll');
      $('.overlay').removeClass('active');
      $('.slide-menu').removeClass('active');
  });

  $("html").click(function(a) {
      if (!$(a.target).parents().is(".menu") && !$(a.target).is(".slide-menu") && !$(a.target).parents().is(".slide-menu")) {
          $('body').removeClass('no-scroll');
          $('.overlay').removeClass('active');
          $('.slide-menu').removeClass('active');
      }
  });

  $('.fixed-top a').click(function() {
    $('html, body').animate({scrollTop: 0}, 500);
  });

  $(window).scroll(function() {
      var a = $(window).scrollTop();
      if (a >= 10) {
          $('header').addClass('scroll');
          $('body').addClass('scroll');
      } else {
          $('header').removeClass('scroll');
          $('body').removeClass('scroll');
      }
  });

  var config = {
    reset: false,
    mobile: false,
  }
  window.sr = ScrollReveal(config);

  for (var index = 1; index < 11; index++) {
      // fade in up
      sr.reveal('.sr-up-td' + index, {
          delay: 400 * index,
          scale: 1,
          duration: 1000,
          distance: '100px',
      });
      // fade in down
      sr.reveal('.sr-down-td' + index, {
          delay: 100 * index,
          scale: 1,
          distance: '-40px',
      });
      // fade in left
      sr.reveal('.sr-left-td' + index, {
          delay: 400 * index,
          scale: 1,
          origin: 'left',
          distance: '100px',
          duration: 1000,
      });
      // fade in right
      sr.reveal('.sr-right-td' + index, {
          delay: 400 * index,
          scale: 1,
          origin: 'right',
          distance: '100px',
          duration: 1000,
      });
  }

  $('nav.nav-menu a').on('click', function() {
      var scrollAnchor = $(this).attr('data-scroll'),
          scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 10;
      $('body,html').animate({
          scrollTop: scrollPoint
      }, 500);
      $('body').removeClass('no-scroll');
      $('.overlay').removeClass('active');
      $('.slide-menu').removeClass('active');
      return false;
  });

  $('nav.nav-menu2 a').on('click', function() {
      var scrollAnchor = $(this).attr('data-scroll'),
          scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 40;
      $('body,html').animate({
          scrollTop: scrollPoint
      }, 500);
      $('body').removeClass('no-scroll');
      $('.overlay').removeClass('active');
      $('.slide-menu').removeClass('active');
      return false;
  });
  
  $('.main-menu nav a').click(function(event) {
    $('.main-menu nav a').removeClass('active');
    $(this).addClass('active');
  });

  $('.click-kontak-kami').click(function(event) {
      $('html,body').animate({
          scrollTop: $('#kontak-kami').offset().top + 20
      },'slow');
  });

  scrollAll();

  $(window).scroll(function() {
      scrollAll();
  });
});